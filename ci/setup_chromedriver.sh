#!/bin/bash

# pre-requisites
apt-get install -y unzip

# download and unzip the chrome driver
VERSION=$(curl http://chromedriver.storage.googleapis.com/LATEST_RELEASE)
curl http://chromedriver.storage.googleapis.com/$VERSION/chromedriver_linux64.zip -O
unzip chromedriver_linux64.zip -d $PWD/bin
